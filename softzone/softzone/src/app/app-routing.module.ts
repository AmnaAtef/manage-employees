import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import {IndexComponent, ContactComponent, 
  AboutComponent, MissionComponent,
  ProjectsComponent, TestimonialsComponent, ServiceComponent, ProjectDetailsComponent } from './pages/index';
import { WebDevelopmentComponent, MobileAppsComponent, WebDesignComponent, DesktopApplicationsComponent} from './pages/service/components/services';
const routes: Routes = [
  { path: '', redirectTo: 'Home', pathMatch: 'full' },
  { path: 'Home', component: IndexComponent },
  { path: 'Contact', component: ContactComponent},
  { path: 'about', component: AboutComponent},
  { path: 'MissionVision', component: MissionComponent},
  { path: 'Projects', component: ProjectsComponent},
  { path: 'Project-Details/:id', component: ProjectDetailsComponent},
  { path: 'Testimonials', component: TestimonialsComponent},
  { path: 'Services', component: ServiceComponent},
  { path: 'Web-Development', component: WebDevelopmentComponent},
  { path: 'Mobile-Apps', component: MobileAppsComponent},
  { path: 'Web-Design', component: WebDesignComponent},
  { path: 'Desktop-Applications', component: DesktopApplicationsComponent},
  // { path: '**', component: NotFoundComponent, data: { pageTitle: 'Not Found' } },

];


@NgModule({
  imports: [RouterModule.forRoot(routes, {
    preloadingStrategy: PreloadAllModules, scrollPositionRestoration: 'enabled'
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
