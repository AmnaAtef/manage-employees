import { Component, OnInit } from '@angular/core';
import { About } from '../../../shared/models';
import { GlobalApiService } from '../../../shared/services/global-api.service';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {

  about = new About;

  constructor(
    private service: GlobalApiService
  ) { }

  ngOnInit(): void {
    this.getAbout();
  }

  getAbout() {
    this.service.GetAbout().subscribe(res => {
      this.about = res.data[0];
    })
  }

}
