import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-testimonials',
  templateUrl: './testimonials.component.html',
  styleUrls: ['./testimonials.component.scss']
})
export class TestimonialsComponent implements OnInit {

  sliders = [
    {imageUrl:"../../../../assets/images/clients/1.png", des:"Soft Zone have done an excellent job presenting the analysisand insights. I am confident in saying that they have helped extremely which toil and pain can procure him some great pleasure."},
    {imageUrl:"../../../../assets/images/clients/2.png", des:"Soft Zone have done an excellent job presenting the analysisand insights. I am confident in saying that they have helped extremely which toil and pain can procure him some great pleasure."},
    {imageUrl:"../../../../assets/images/clients/3.png", des:"Soft Zone have done an excellent job presenting the analysisand insights. I am confident in saying that they have helped extremely which toil and pain can procure him some great pleasure."},
    {imageUrl:"../../../../assets/images/clients/4.png", des:"Soft Zone have done an excellent job presenting the analysisand insights. I am confident in saying that they have helped extremely which toil and pain can procure him some great pleasure." },
  ]
  
  constructor() { }

  ngOnInit(): void {
  }

}
