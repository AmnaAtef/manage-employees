import { Component, OnInit } from '@angular/core';
import { Projects } from '../../../shared/models';
import { GlobalApiService } from '../../../shared/services/global-api.service';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: []
})
export class ProjectsComponent implements OnInit {

  projects : Projects[];

  constructor(
    private service: GlobalApiService
  ) { }

  ngOnInit(): void {
    this.getProjects();
  }

  getProjects() {
    this.service.GetProjects().subscribe(res => {
      this.projects = res.data;
    })
  }
}
