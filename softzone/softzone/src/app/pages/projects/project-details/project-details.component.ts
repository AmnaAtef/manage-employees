import { Component, OnInit } from '@angular/core';
import { Projects } from '../../../shared/models';
import { GlobalApiService } from '../../../shared/services/global-api.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-project-details',
  templateUrl: './project-details.component.html',
  styleUrls: ['./project-details.component.scss']
})
export class ProjectDetailsComponent implements OnInit {

  images = [
    {imageUrl:"../../../../assets/images/gallery/1.jpg"},
    {imageUrl:"../../../../assets/images/gallery/2.jpg"},
    {imageUrl:"../../../../assets/images/gallery/3.jpg"},
    {imageUrl:"../../../../assets/images/gallery/4.jpg"},
    {imageUrl:"../../../../assets/images/gallery/5.jpg"},
    {imageUrl:"../../../../assets/images/gallery/6.jpg"},
  ];
  responsiveOptions;
  id
  project= new Projects;
  constructor(
    private service: GlobalApiService,
    private route: ActivatedRoute
  )
   { 
    this.responsiveOptions = [
      {
          breakpoint: '1024px',
          numVisible: 5
      },
      {
          breakpoint: '768px',
          numVisible: 3
      },
      {
          breakpoint: '560px',
          numVisible: 1
      }
  ];
  }

  ngOnInit(): void {
    this.route.snapshot.params.id; //projectId
    
    this.getProjectById();
  }

  getProjectById() {
    this.service.GetProjectDetails(this.route.snapshot.params.id).subscribe(res => {
      this.project = res.data
    
    })

  }
}
