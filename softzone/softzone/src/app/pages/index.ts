export * from "./about/about/about.component";
export * from "./contact/contact.component";
export * from "./index/index.component";
export * from "./mission/mission/mission.component";
export * from "./projects/projects/projects.component";
export * from "./service/service.component";
export * from "./testimonials/testimonials/testimonials.component";
export * from "./projects/project-details/project-details.component";