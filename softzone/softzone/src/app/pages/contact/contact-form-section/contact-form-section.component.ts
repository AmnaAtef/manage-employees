import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ValidationAfterSubmit } from '../../../shared/plugins';
import { PostContactUS } from '../../../shared/models';
import { Observable, from } from 'rxjs';
import { GlobalApiService } from '../../../shared/services/global-api.service';
import {MessageService} from 'primeng/api';
@Component({
  selector: 'app-contact-form-section',
  templateUrl: './contact-form-section.component.html',
  styleUrls: ['./contact-form-section.component.scss'],
  providers: [MessageService]
})
export class ContactFormSectionComponent implements OnInit {

  submitted = false;
  contactMessage = new PostContactUS;
  ContactUsForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private postContactService: GlobalApiService, 
    private isFormValid: ValidationAfterSubmit,
    private messageService: MessageService
  ) { }

  ngOnInit(): void {
    this.initForm()
  }

  private initForm() {
    this.ContactUsForm = this.fb.group({
      user_name: [null, [Validators.required, Validators.pattern("[a-zA-Zء-ي ]+$")]],
      user_email: [null, [Validators.required, Validators.email, Validators.pattern("^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{3}$")]],
      inquiry_subject:[null, Validators.required],
      inquiry_msg: [null, [Validators.required, Validators.pattern("^[a-zA-Z0-9ء-ي., ]*$"), ]],
      phone:[null,[ Validators.required, Validators.pattern("^(\\+\\d{1,3}[- ]?)?\\d{8,14}$")]],
      inquiry_type_id: 3
     

    });
  }
  get f() {
    return this.ContactUsForm.controls;
  }

  sendContactMessage() {
    this.contactMessage = this.ContactUsForm.value;
    this.isFormValid.afterSubmit(this.ContactUsForm);
    this.submitted = true;

    if (this.ContactUsForm.valid == true) {
      this.postContactService.postContactUS(this.contactMessage).subscribe(res => {
        if (res.succeeded == true) {
          this.ContactUsForm.patchValue({
            user_name: null,
            user_email: null,
            inquiry_subject: null,
            inquiry_msg: null,
            phone: null
          });

          this.submitted = false;
          this.ContactUsForm.reset()
          this.messageService.add({severity:'success',
          summary: 'Success', detail: 'Thanks, Your Message sent Successfully'});
        }
       
        else {
          this.messageService.add({severity:'error',
          summary: 'Error', detail: 'Please Check Your Feild Again'});
        }
      })

    }
  }
}
