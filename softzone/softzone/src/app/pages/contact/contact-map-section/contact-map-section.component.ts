import { Component, OnInit } from '@angular/core';
import { GetContactUS } from '../../../shared/models';
import { GlobalApiService } from '../../../shared/services/global-api.service';

@Component({
  selector: 'app-contact-map-section',
  templateUrl: './contact-map-section.component.html',
  styleUrls: ['./contact-map-section.component.scss']
})
export class ContactMapSectionComponent implements OnInit {

  contacts = new GetContactUS;
 
  constructor(
    private service: GlobalApiService
  ) { }

  ngOnInit(): void {
    this.getdetails();
  }

  getdetails() {
    this.service.getContactUS().subscribe(res => {
      this.contacts = res.data[0];
    }) 
  }

}
