import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContactMapSectionComponent } from './contact-map-section.component';

describe('ContactMapSectionComponent', () => {
  let component: ContactMapSectionComponent;
  let fixture: ComponentFixture<ContactMapSectionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContactMapSectionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactMapSectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
