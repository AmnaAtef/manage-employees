import { Component, OnInit } from '@angular/core';
import { GetContactUS } from '../../../shared/models';
import { GlobalApiService } from '../../../shared/services/global-api.service';
import { ViewportScroller} from '@angular/common';

@Component({
  selector: 'app-in-touch-section',
  templateUrl: './in-touch-section.component.html',
  styleUrls: ['./in-touch-section.component.scss']
})
export class InTouchSectionComponent implements OnInit {

  contacts = new GetContactUS;
  phones;
  phoneList;
 
  constructor(
    private service: GlobalApiService,
    private scroll: ViewportScroller
    ) { }

  ngOnInit(): void {
    this.getdetails();
  }

  getdetails() {
    this.service.getContactUS().subscribe(res => {
      this.contacts = res.data[0];

      this.phones = this.contacts.phone_lst.slice(0, -1).split(","); // convert string phones to array and remove last comma
      this.phoneList = this.phones.join(" , "); //separate items with " , " 
    }) 
  }
  scrollFn(anchor: string): void{
  	this.scroll.scrollToAnchor(anchor)
}
}
