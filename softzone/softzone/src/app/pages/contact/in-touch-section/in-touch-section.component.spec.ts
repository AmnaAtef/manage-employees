import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InTouchSectionComponent } from './in-touch-section.component';

describe('InTouchSectionComponent', () => {
  let component: InTouchSectionComponent;
  let fixture: ComponentFixture<InTouchSectionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InTouchSectionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InTouchSectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
