export * from "./web-development/web-development.component";
export * from "./mobile-apps/mobile-apps.component";
export * from "./web-design/web-design.component";
export * from "./desktop-applications/desktop-applications.component";
export * from "./sidebar-side/sidebar-side.component";