import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DesktopApplicationsComponent } from './desktop-applications.component';

describe('DesktopApplicationsComponent', () => {
  let component: DesktopApplicationsComponent;
  let fixture: ComponentFixture<DesktopApplicationsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DesktopApplicationsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DesktopApplicationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
