import { Component, OnInit } from '@angular/core';
import { Slider } from '../../../../../shared/models';
import { GlobalApiService } from '../../../../../shared/services/global-api.service';

@Component({
  selector: 'app-banner-section',
  templateUrl: './banner-section.component.html',
  styleUrls: ['./banner-section.component.scss']
})
export class BannerSectionComponent implements OnInit {
 
 
  sliders : Slider[];
  constructor(
    private service: GlobalApiService
  ) { }

  ngOnInit(): void {
    this.getSlider();
  }

  getSlider() {
    this.service.GetSlider().subscribe(res => {
      this.sliders = res.data;
    })
  }
}
