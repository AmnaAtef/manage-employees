import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LatestProjectsSectionComponent } from './latest-projects-section.component';

describe('LatestProjectsSectionComponent', () => {
  let component: LatestProjectsSectionComponent;
  let fixture: ComponentFixture<LatestProjectsSectionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LatestProjectsSectionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LatestProjectsSectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
