export * from "./banner-section/banner-section/banner-section.component";
export * from "./business-section/business-section/business-section.component";
export * from "./industry-section/industry-section/industry-section.component";
export * from "./featured-section/featured-section.component";
export * from "./latest-projects-section/latest-projects-section.component";
export * from "./mobile-application-section/mobile-application-section/mobile-application-section.component";
export * from "./projects-section/projects-section/projects-section.component";
export * from "./testimonial-section/testimonial-section/testimonial-section.component";