import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MobileApplicationSectionComponent } from './mobile-application-section.component';

describe('MobileApplicationSectionComponent', () => {
  let component: MobileApplicationSectionComponent;
  let fixture: ComponentFixture<MobileApplicationSectionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MobileApplicationSectionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MobileApplicationSectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
