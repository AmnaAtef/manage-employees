import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-testimonial-section',
  templateUrl: './testimonial-section.component.html',
  styleUrls: ['./testimonial-section.component.scss']
})
export class TestimonialSectionComponent implements OnInit {
  
  sliders = [
    {imageUrl:"../../../../assets/images/clients/1.png", des:"Soft Zone have done an excellent job presenting the analysisand insights. I am confident in saying that they have helped extremely which toil and pain can procure him some great pleasure."},
    {imageUrl:"../../../../assets/images/clients/2.png", des:"Soft Zone have done an excellent job presenting the analysisand insights. I am confident in saying that they have helped extremely which toil and pain can procure him some great pleasure."},
    {imageUrl:"../../../../assets/images/clients/3.png", des:"Soft Zone have done an excellent job presenting the analysisand insights. I am confident in saying that they have helped extremely which toil and pain can procure him some great pleasure."},
    {imageUrl:"../../../../assets/images/clients/4.png", des:"Soft Zone have done an excellent job presenting the analysisand insights. I am confident in saying that they have helped extremely which toil and pain can procure him some great pleasure." },
  ]
  constructor() { }

  ngOnInit(): void {
  }

}
