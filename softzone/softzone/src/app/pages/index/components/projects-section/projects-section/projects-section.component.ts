import { Component, OnInit } from '@angular/core';
import { Projects } from '../../../../../shared/models';
import { GlobalApiService } from '../../../../../shared/services/global-api.service';

@Component({
  selector: 'app-projects-section',
  templateUrl: './projects-section.component.html',
  styleUrls: ['./projects-section.component.scss']
})
export class ProjectsSectionComponent implements OnInit {

  projects : Projects[];
  sliders = [
    {imageUrl:"../../../../assets/images/gallery/1.jpg"},
    {imageUrl:"../../../../assets/images/gallery/2.jpg"},
    {imageUrl:"../../../../assets/images/gallery/3.jpg"},
    {imageUrl:"../../../../assets/images/gallery/4.jpg"},
    {imageUrl:"../../../../assets/images/gallery/5.jpg"},
    {imageUrl:"../../../../assets/images/gallery/6.jpg"},
  ];
  responsiveOptions;
  constructor(
    private service: GlobalApiService
  ) { 
    this.responsiveOptions = [
      {
          breakpoint: '1024px',
          numVisible: 3,
          numScroll: 3
      },
      {
          breakpoint: '768px',
          numVisible: 2,
          numScroll: 2
      },
      {
          breakpoint: '560px',
          numVisible: 1,
          numScroll: 1
      }
  ];
  }

  ngOnInit(): void {
    this.getProjects();
  }

  getProjects() {
    this.service.GetProjects().subscribe(res => {
      this.projects = res.data;
    })
  }
}
