import { Component, OnInit } from '@angular/core';
import { About } from '../../../shared/models';
import { GlobalApiService } from '../../../shared/services/global-api.service';

@Component({
  selector: 'app-mission',
  templateUrl: './mission.component.html',
  styleUrls: ['./mission.component.scss']
})
export class MissionComponent implements OnInit {

  mission_vision = new About;

  constructor(
    private service: GlobalApiService
  ) { } 

  ngOnInit(): void {
    this.getMissionVision();
  }

  getMissionVision() {
    this.service.GetAbout().subscribe(res => {
      this.mission_vision = res.data[0];
    })
  }
}
