import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { NgwWowModule } from 'ngx-wow';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AgmCoreModule } from '@agm/core';

import { 
  AboutComponent,
  ContactComponent,
  MissionComponent,
  ProjectsComponent,
  ServiceComponent,
  TestimonialsComponent,
  IndexComponent,
  ProjectDetailsComponent
} from './index';
import { 
  BusinessSectionComponent,
  IndustrySectionComponent,
  ProjectsSectionComponent,
  BannerSectionComponent,
  TestimonialSectionComponent,
  MobileApplicationSectionComponent,
  LatestProjectsSectionComponent,
  FeaturedSectionComponent,
} from './index/components/components';
import {
  SidebarSideComponent,
  WebDevelopmentComponent,
  MobileAppsComponent,
  WebDesignComponent,
  DesktopApplicationsComponent
} from './service/components/services';
import { ContactTitleComponent } from './contact/components/contact-title/contact-title.component';
import { InTouchSectionComponent } from './contact/in-touch-section/in-touch-section.component';
import { ContactFormSectionComponent } from './contact/contact-form-section/contact-form-section.component';
import { ContactMapSectionComponent } from './contact/contact-map-section/contact-map-section.component';


const COMPONENTS = [
  AboutComponent,
  ContactComponent,
  MissionComponent,
  ProjectsComponent,
  ServiceComponent,
  TestimonialsComponent,
  IndexComponent,
  ProjectDetailsComponent,

  BusinessSectionComponent,
  IndustrySectionComponent,
  ProjectsSectionComponent,
  BannerSectionComponent,
  TestimonialSectionComponent,
  MobileApplicationSectionComponent,
  LatestProjectsSectionComponent,
  FeaturedSectionComponent,

  SidebarSideComponent,
  WebDevelopmentComponent,
  MobileAppsComponent,
  WebDesignComponent,
  DesktopApplicationsComponent
]
@NgModule({
  declarations: [
    ...COMPONENTS,
    ContactTitleComponent,
    InTouchSectionComponent,
    ContactFormSectionComponent,
    ContactMapSectionComponent,

  ],
  imports: [
    CommonModule,
    SharedModule,
    NgwWowModule,
    RouterModule,
    NgbModule,
    ReactiveFormsModule, 
    FormsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyACzBGgLOpi6jxeOm2Q74xYAT7Ajuco5NM'
    })
  ],
  exports: [...COMPONENTS],
})
export class PagesModule { }
