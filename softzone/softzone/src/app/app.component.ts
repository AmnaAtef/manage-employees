import { Component } from '@angular/core';
import { NgwWowService } from 'ngx-wow';
import { ViewportScroller} from '@angular/common';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'SOFT ZONE';
  constructor(private wowService: NgwWowService,
    private scroll: ViewportScroller) {
    this.wowService.init();
  }

  scrollToTop(){
    this.scroll.scrollToPosition([0,0]);
  }
}
