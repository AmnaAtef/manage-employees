import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {CarouselModule} from 'primeng/carousel';
import {GalleriaModule} from 'primeng/galleria';
import {MessagesModule} from 'primeng/messages';
import {MessageModule} from 'primeng/message';
import {ToastModule} from 'primeng/toast';

const PRIMENG_IMPORTS = [
  CarouselModule,
  GalleriaModule,
  MessagesModule,
  MessageModule,
  ToastModule
]


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ...PRIMENG_IMPORTS
  ],
  exports: [
    ...PRIMENG_IMPORTS
  ],
  providers: []
})
export class PrimengImportsModule { }
