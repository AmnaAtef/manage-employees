import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PrimengImportsModule } from './modules';
import { RouterModule } from '@angular/router';
import { HeaderComponent } from './components/header/header/header.component';
import { FooterComponent } from './components/footer/footer/footer.component';
import { ContactInfoComponent } from './components/contact-info/contact-info/contact-info.component';



@NgModule({
  declarations: [
    HeaderComponent,
    FooterComponent,
    ContactInfoComponent
  ],
  imports: [
    CommonModule,
    PrimengImportsModule,
    RouterModule,
  ],
  exports: [
    HeaderComponent,
    FooterComponent,
    ContactInfoComponent,
    PrimengImportsModule,
  ],
})
export class SharedModule { }
