import { Component, OnInit } from '@angular/core';
import { GetContactUS } from '../../../models/GetContactUs.model';
import { GlobalApiService } from '../../../services/global-api.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  isOpen: boolean = false;
  contacts = new GetContactUS;
  constructor(
    private service: GlobalApiService
  ) { }

  ngOnInit(): void {
    this.getdetails();
  }

  on() {
    this.isOpen = !this.isOpen
  }

  getdetails() {
    this.service.getContactUS().subscribe(res => {
      this.contacts = res.data[0];
    }) 
  }
}
