import { Component, OnInit } from '@angular/core';
import { GetContactUS} from '../../../models';
import { GlobalApiService } from '../../../services/global-api.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  contacts = new GetContactUS;
  openAbout: boolean = false;
  openServices: boolean = false;
  
  constructor(
    private service: GlobalApiService
  ) { }

  ngOnInit(): void {
    this.getdetails();
  }

  getdetails() {
    this.service.getContactUS().subscribe(res => {
      this.contacts = res.data[0];
    }) 
  }
  onAbout() {
    this.openAbout = !this.openAbout
  }
  onServices() {
    this.openServices = !this.openServices
  }

}
