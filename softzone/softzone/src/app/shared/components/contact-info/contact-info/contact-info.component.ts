import { Component, OnInit } from '@angular/core';
import { GetContactUS, About } from '../../../models';
import { GlobalApiService } from '../../../services/global-api.service';

@Component({
  selector: 'app-contact-info',
  templateUrl: './contact-info.component.html',
  styleUrls: ['./contact-info.component.scss']
})
export class ContactInfoComponent implements OnInit {

  contacts = new GetContactUS;
  about = new About;
  splitPhone;
  phones;
  fax;
  phoneList;
  constructor(
    private service: GlobalApiService
  ) { }

  ngOnInit(): void {
    this.getdetails();
    this.getAbout();
  }

  getdetails() {
    this.service.getContactUS().subscribe(res => {
      this.contacts = res.data[0];

      this.fax = this.contacts.fax_lst.slice(0, -1).split(",").join(" , ");
      this.phones = this.contacts.phone_lst.slice(0, -1).split(","); // convert string phones to array and remove last comma
      this.phoneList = this.phones.join(" , "); //separate items with " , " 
    }) 
  }
  getAbout() {
    this.service.GetAbout().subscribe(res => {
      this.about = res.data[0];
    })
  }
}
