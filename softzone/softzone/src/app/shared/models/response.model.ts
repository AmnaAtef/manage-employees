export class Response<T> {
    errors?: string;
    message?: string;
    data?:T;
    succeeded?: boolean;
}