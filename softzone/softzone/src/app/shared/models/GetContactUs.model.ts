export class GetContactUS {
    latitute: string;
    longitute: string;
    address_ar: string;
    address_en: string;
    phone_lst: any;
    fax_lst: any;
    fb_link: string;
    twitter_link: string;
    youtube_link: string;
    email?:string;
    linked_link: string;
    website_link: string;
} 
