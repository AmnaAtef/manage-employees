export class Projects {
  news_id?: number;
  news_title?: string;
  news_content?: string;
  img_name?: string;
  news_images?: NewsImages;
  data?: Projects;
} 
export class NewsImages {
  title?: string;
  img_name?: string;
}