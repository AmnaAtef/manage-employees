export class PostContactUS {
  user_email?: string;
  user_name?: string;
  inquiry_subject?: string;
  inquiry_msg?: string;
  user_id: number;
  inquiry_type_id: number; 
  inquiry_att_lst: string;
  lang_id: number;

} 