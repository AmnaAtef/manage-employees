import { Injectable } from '@angular/core';
import { EndPoints } from './endpoints';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { GetContactUS,PostContactUS, Response, About, Slider, Projects } from '../models';

@Injectable({
  providedIn: 'root'
})
export class GlobalApiService {
  private GetSliderApiUrl = EndPoints.GetSlider_ENDPOINT;
  private PostContactUSApiUrl = EndPoints.PostContactUS_ENDPOINT;
  private GetContactUsApiUrl = EndPoints.GetContactUs_ENDPOINT;
  private GetAboutApiUrl = EndPoints.GetAbout_ENDPOINT;
  private GetProjectsApiUrl = EndPoints.GetProjects_ENDPOINT;
  private GetProjects_detailsApiUrl = EndPoints.GetProjects_details_ENDPOINT;

  constructor(private http: HttpClient, private router: Router) { }

  // CONTACT US PAGE
  getContactUS(): Observable<Response<GetContactUS>> {
    return this.http.get<Response<GetContactUS>>(this.GetContactUsApiUrl);
  }
  postContactUS(sendMessage): Observable<Response<PostContactUS>> {
    return this.http.post<Response<PostContactUS>>(this.PostContactUSApiUrl, sendMessage);
  }
  // ABOUT US 
  GetAbout(): Observable<Response<About>> {
    return this.http.get<Response<About>>(this.GetAboutApiUrl);
  }

  // SLIDER
  GetSlider(): Observable<Response<Slider[]>> {
    return this.http.get<Response<Slider[]>>(this.GetSliderApiUrl);
  }

  // PROJECTS
  GetProjects(): Observable<Response<Projects[]>> {
    return this.http.get<Response<Projects[]>>(this.GetProjectsApiUrl);
  }

   // PROJECT DETAILS
   GetProjectDetails(id: number): Observable<Response<Projects>> {
    return this.http.get<Response<Projects>>(`${this.GetProjects_detailsApiUrl}?news_id=${id}`);
  }

}
