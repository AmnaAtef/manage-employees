
const BASE_URL = "http://81.10.12.75:151";
const API = "/api";
const V1 = "/v1";
const WEBSITE = "/Website";
const API_URL = BASE_URL + API + V1 + WEBSITE;

const GetAbout = "/GetAbout";
const GetContactUs = "/GetContactUs";
const PostContactUS = "/PostContactUS";
const GetSlider = "/GetSlider";
const GetProjects = "/Get_News";
const GetProjects_details = "/GetNews_details";


export abstract class EndPoints {
    public static BASE_URL = BASE_URL;
    public static API_URL = API_URL;
    public static GetSlider_ENDPOINT = API_URL+ GetSlider;
    public static GetAbout_ENDPOINT = API_URL + GetAbout;
    public static GetContactUs_ENDPOINT = API_URL +GetContactUs;
    public static PostContactUS_ENDPOINT = API_URL +PostContactUS;
    public static GetProjects_ENDPOINT = API_URL +GetProjects;
    public static GetProjects_details_ENDPOINT = API_URL + GetProjects_details;
}
