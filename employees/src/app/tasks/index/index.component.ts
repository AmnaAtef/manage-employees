import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../../shared/services/api';
import { Task } from '../../shared/models';
import {ConfirmationService} from 'primeng/api';
import {Message} from 'primeng/api';
import {MessageService} from 'primeng/api';
import {DialogService} from 'primeng/dynamicdialog';
import {AddComponent} from '../add/add.component';
import { EditComponent} from "../edit/edit.component";

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss'],
  providers: [ConfirmationService, MessageService, DialogService],
})
export class IndexComponent implements OnInit {
   
  list: Task[];
  totalRecords: number;
  msgs: Message[] = [];
  displayBasic: boolean;
  constructor(
    private service: EmployeeService,
    private confirmationService: ConfirmationService,
    private messageService: MessageService,
    public dialogService: DialogService
  ) { }

  ngOnInit(): void {
    this.getAllTasks();
  }
  getAllTasks(){
    this.service.getallTasks().subscribe(res =>{
      this.list = res
    } )
  }

  deleteTask(Id) {
    this.confirmationService.confirm({
      message: 'Do you want to delete this Task?',
      header: 'Delete Confirmation',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.service.deleteTask(Id).subscribe(res=>{
              // if(res.status==200){
                let index= this.list.findIndex(e => e.Id === Id)
                      if(index !== -1){
                        this.list.splice(index,1)
                      }
                this.messageService.add({severity:'success', summary:'Successful', detail:'Task Deleted'});
              // }
            })   
      } 
  });
}
addTaskDialog(){
  this.displayBasic = true;
  const ref = this.dialogService.open(AddComponent, {
    header: "Add New Task",
    width: '30%'
});
}
editTaskDialog(id){
  const ref = this.dialogService.open(EditComponent, {
    header: "Edit Task",
    width: '30%',
    data: {id: id}
});
}

}
