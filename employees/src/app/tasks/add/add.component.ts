import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ValidationAfterSubmit } from '../../shared/plugins';
import { Task } from '../../shared/models/task.model';
import { Observable, from } from 'rxjs';
import { EmployeeService } from '../../shared/services/api';
import {MessageService} from 'primeng/api';
@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss'],
  providers: [MessageService]
})
export class AddComponent implements OnInit {

  submitted = false;
  Task = new Task;
  TaskForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private taskService: EmployeeService, 
    private isFormValid: ValidationAfterSubmit,
    private messageService: MessageService
  ) { }

  ngOnInit(): void {
    this.initForm()
  }

  private initForm() {
    this.TaskForm = this.fb.group({
      Name: [null, [Validators.required, Validators.pattern("[a-zA-Z][a-zA-Z ]{2,}")]],
      Description: [null, Validators.required],
      Url:[null, Validators.required],
      UserName: [null, Validators.required],
      Password: [null, Validators.required],
      WorkImg: [null, Validators.required],

    });
  }
  get f() {
    return this.TaskForm.controls;
  }

  addTask() {
    this.Task = this.TaskForm.value;
    this.isFormValid.afterSubmit(this.TaskForm);
    this.submitted = true;

    if (this.TaskForm.valid == true) {
      this.taskService.createTask(this.Task).subscribe(res => {
        // if (res.status == 200) {
          this.TaskForm.patchValue({
            Name: null,
            Description: null,
            Url: null,
            UserName: null,
            Password: null,
            WorkImg: null
          });

          this.submitted = false;
          this.TaskForm.reset()
          this.messageService.add({severity:'success',
          summary: 'Success', detail: 'Task added Successfully'});
          setTimeout(() =>{
            window.location.reload();
          }, 1000);
        // }
       
        // else {
          // this.messageService.add({severity:'error',
          // summary: 'Error', detail: 'Please Check Your Feild Again'});
        // }
      })

    }
  }

}
