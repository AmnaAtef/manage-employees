
const BASE_URL = "https://taskapiangular.azurewebsites.net";
const API_URL = BASE_URL + "/api";
const Myworks = "/Myworks";
const ALL_TASKS_URL = "/Get";
const ADD_NEW_TASK_URL = "/Post";
const DELETE_URL = "/Delete";
const EDIT_TASK_URL = "/Put";
const GetById_TASK_URL = "/GetById";

export abstract class EndPoints {
    public static BASE_URL = BASE_URL;
    public static API_URL = API_URL;
    public static ALL_TASKS_ENDPOINT = API_URL + Myworks + ALL_TASKS_URL;
    public static NEW_TASK_ENDPOINT = API_URL + Myworks + ADD_NEW_TASK_URL;
    public static DELETE_ENDPOINT = API_URL + Myworks + DELETE_URL;
    public static EDIT_TASK_ENDPOINT = API_URL + Myworks + EDIT_TASK_URL;
    public static GetById_TASK_ENDPOINT = API_URL + Myworks + GetById_TASK_URL;
    
   
}
